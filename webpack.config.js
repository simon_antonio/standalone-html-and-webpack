const Webpack = require('webpack');
const Path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const AssetsPlugin = require('assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = env => {
    // Constants for environment/mode configurations
    const { PLATFORM, STATS, PORT, SITE, IIS, SECURE, HOST } = env;

    const reverseProxy = env.IIS;
    const devHost = env.HOST ? env.HOST : env.IIS;
    const useHttps = env.SECURE == 'true' ? true : false;
    const nodeEnv = env.NODE_ENV;
    const isProduction = PLATFORM === 'production';
    const isDebug = PLATFORM === 'development';
    const showStats = STATS === 'show';

    console.log(' ');
    console.log('-------------------------------------------');
    console.log('isProduction', isProduction ? 'true' : 'false');
    console.log('isDebug', isDebug ? 'true' : 'false');
    console.log('isSecure', useHttps ? 'true' : 'false', env.SECURE, env.SECURE == 'true');
    console.log('-------------------------------------------');
    console.log(' ');

	const relativePublicPath = '/';//i.e if the app runs from outside of assets leave this blank,

    const plugins = [
        // Make the WebpackHelper.cs magic work - include the files with hashes
        new AssetsPlugin({
            filename: 'webpack.assets.json',
            path: 'assets/dist', //where to put this
            prettyPrint: true,
            metadata: {
                ProductionBuild: isProduction,
            },
        }),
        new HtmlWebpackPlugin({
			template: './html/index.html',
			filename: 'index.html',
        }),
    ];

    if (isProduction) {
        plugins.push(
            new MiniCssExtractPlugin({
                filename: '[name].[hash].css',
                chunkFilename: '[id].css',
            })
        );

        //plugins.push(new CleanWebpackPlugin());
        plugins.push(new OptimizeCssAssetsPlugin());
    }

    if (isDebug) {
        // Add console progress bar
        plugins.push(new Webpack.ProgressPlugin());
    }

    if (showStats) {
        // Generates visual representation of bundle sizes
        plugins.push(new BundleAnalyzerPlugin());
    }

    return {
        // The folder to base file paths on as its root
        context: Path.resolve(__dirname, 'assets'),

        entry: {
            // Project
            app: './scripts/index.js',
        },

        output: {
            filename: chunkData => {
                if (isProduction) {
                    return '[name]-bundle.[hash].js';
                } else {
                    return '[name]-bundle.js';
                }
            },
            // Use contenthash for vendor packages, so that these are only re-downloaded when the content changes
            chunkFilename: '[name].[contenthash].js',
            path: Path.resolve(__dirname, 'assets/dist'),
            publicPath: relativePublicPath,//'/assets/dist/'
        },

        module: {
            rules: [
                // Config in .babelrc
                {
                    test: /.(js|jsx)$/,
                    exclude: /(node_modules|bcore-js\b|@babel\b)/,
                    use: 'babel-loader',
                },
                {
                    test: /\.(sa|sc|c)ss$/,
                    use: [
                        // Serve CSS via JavaScript while debugging
                        isProduction ? { loader: MiniCssExtractPlugin.loader } : { loader: 'style-loader' },
                        { loader: 'css-loader', options: { sourceMap: true, importLoaders: 3 } },
                        // Refer to postcss.config.js for more
                        { loader: 'postcss-loader', options: { sourceMap: true } },
                        { loader: 'sass-loader', options: { sourceMap: true } },
                    ],
                },
                {
                    test: /\.(gif|svg|jpg|png)$/,
                    loader: 'file-loader',
                },
                // Automatically optimises SVGs for inline conversion
                {
                    test: /\.svg$/,
                    use: [
                        { loader: 'file-loader' },
                        {
                            loader: 'svgo-loader',
                            options: {
                                plugins: [{ removeViewBox: false }, { removeDimensions: true }],
                            },
                        },
                    ],
                },
            ],
        },

        optimization: {
            minimizer: [
                new TerserPlugin({
                    extractComments: true, // Summarizes comments into one file
                    terserOptions: {
                        warnings: false,
                        parse: {},
                        sourceMap: true,
                        compress: {
                            drop_console: true,
                        },
                        mangle: true, // Note `mangle.properties` is `false` by default.
                        output: null,
                        toplevel: false,
                        nameCache: null,
                        ie8: false,
                        keep_fnames: false,
                    },
                }),
            ],
            splitChunks: {
                chunks: 'all',
                maxInitialRequests: 20,
                minSize: 0,
                cacheGroups: {
                    vendor: {
                        test: /[\\/]node_modules[\\/]/,
                        name(module) {
                            // get the name. E.g. node_modules/packageName/not/this/part.js
                            // or node_modules/packageName
                            const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];

                            // npm package names are URL-safe, but some servers don't like @ symbols
                            return `vendor.${packageName.replace('@', '')}`;
                        },
                    },
                },
            },
        },

        plugins: plugins,

        // Pretty console
        stats: {
            colors: true,
        },

        // Build source map for debug
        devtool: isDebug ? 'eval-source-map' : 'source-map',

        watchOptions: {
            aggregateTimeout: 300,
            poll: 1000,
            ignored: /node_modules/,
        },

        devServer: {
            // Match the output path
            contentBase: Path.resolve(__dirname, 'assets/dist'),

            // Match the output publicPath
            publicPath: '/assets/dist/',

            watchContentBase: true,
            // proxy: {
            //     '*': {
            //         target: (useHttps ? 'https://' : 'http://') + reverseProxy,
            //         secure: false,
            //     },
            // },
            port: 9069,
            host: devHost,
            https: false,
        },
    };
};
