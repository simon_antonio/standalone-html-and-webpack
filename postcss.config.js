const autoprefixer = require('autoprefixer');
const postcssImport = require('postcss-import');

// If autoprefixer causes issue this one can be switched in
// var postcssPresetEnv = require('postcss-preset-env');

module.exports = {
    sourceMap: true,
    plugins: [
        // Apply prefixes for other browsers
        autoprefixer({}),
        postcssImport({}),
    ],
};
